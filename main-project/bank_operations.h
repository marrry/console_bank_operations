#ifndef BANK_OPERATIONS_H
#define BANK_OPERATIONS_H

#include "constants.h"

struct date
{
    int day;
    int month;
    int year;
};

struct timeop
{
    int hours;
    int minutes;
    int seconds;
};

struct bank_operations
{
    date date;
    timeop time;
    char type[MAX_STRING_SIZE];
    char account[MAX_STRING_SIZE];
    double sum;
    char aim[MAX_STRING_SIZE];
};

#endif