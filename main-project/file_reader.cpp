#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

date convert1(char* str)
{
    date result1;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result1.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result1.month = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result1.year = atoi(str_number);
    return result1;
}

timeop convert2(char* str)
{
    timeop result2;
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    result2.hours = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result2.minutes = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result2.seconds = atoi(str_number);
    return result2;
}

void read(const char* file_name, bank_operations* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            bank_operations* item = new bank_operations;
            file >> tmp_buffer;
            item->date = convert1(tmp_buffer);
            file >> tmp_buffer;
            item->time = convert2(tmp_buffer);
            file >> item->type;
            file >> item->account;
            file >> item->sum;
            file.read(tmp_buffer, 1);
            file.getline(item->aim, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}